import fnmatch

from math import floor, log10
import os
import yaml
import logging
import zipfile

import glob
import io
import bz2
from datetime import datetime
from xml.etree import ElementTree


JSON_INDENT = 4

_BLACKLIST = (
    "__CLOUD__created_files.txt",
    "RunResult-*.zip",
    "*.log.data",
    "*.log.stdError",
    "fileHashes.json",
)
"""List of files that are not hashed. May include wildcard '*' and brackets '[', ']'"""


def is_on_blacklist(filename):
    return any(fnmatch.fnmatch(os.path.basename(filename), b) for b in _BLACKLIST)


def get_file_number_in_zip(zipped_file) -> int:
    """Return the list of files in this zip"""
    try:
        with zipfile.ZipFile(zipped_file) as inp_zip:
            return len(
                [
                    x
                    for x in inp_zip.namelist()
                    if x.endswith(".xml") and not x.endswith("metadata.xml")
                ]
            )
    except FileNotFoundError as e:
        # print('Error: Zip file does not exist.')
        return 0


def round_to_sig_numbers(x: float, n: int) -> float:
    if x == 0:
        return 0
    return round(x, -int(floor(log10(abs(x)))) + (n - 1))


def parse_yaml(yaml_file):
    try:
        with open(yaml_file) as inp:
            return yaml.safe_load(inp)
    except yaml.scanner.ScannerError as e:
        logging.error("Exception while scanning %s", yaml_file)
        raise e


def write_xml_file(output_file, xml):
    if not xml:
        logging.info("No xml for output %s", output_file)
        return
    for run in xml.findall("run"):
        # Clean-up an entry that can be inferred by table-generator automatically, avoids path confusion
        del run.attrib["logfile"]
    xml_string = (
        xml_to_string(xml).decode("utf-8").replace("    \n", "").replace("  \n", "")
    )
    if not xml_string:
        logging.info("No xml for output %s", output_file)
        return
    with io.TextIOWrapper(bz2.BZ2File(output_file, "wb"), encoding="utf-8") as xml_file:
        xml_file.write(xml_string)


def xml_to_string(
    elem,
    qualified_name="result",
    public_id="+//IDN sosy-lab.org//DTD BenchExec result 3.0//EN",
    system_id="https://www.sosy-lab.org/benchexec/result-3.0.dtd",
):
    """
    Return a pretty-printed XML string for the Element.
    Also allows setting a document type.
    """
    from xml.dom import minidom

    rough_string = ElementTree.tostring(elem, "utf-8")
    if not rough_string:
        logging.info("No xml for elem %s", elem)
        return None
    reparsed = minidom.parseString(rough_string)
    if qualified_name:
        doctype = minidom.DOMImplementation().createDocumentType(
            qualified_name, public_id, system_id
        )
        reparsed.insertBefore(doctype, reparsed.documentElement)
    return reparsed.toprettyxml(indent="  ", encoding="utf-8")


def find_latest_file(
    validator: str,
    verifier: str,
    subcategory: str,
    year="22",
    fixed=False,
    output="results-validated",
):
    assert isinstance(year, str) and len(year) == 2, (
        f"Convention demands only the last two digits of the current year "
        "in string format (leading zeros) but got: '{year}'"
    )
    processed_fixed = ".fixed.xml.bz2" if fixed else ""
    prefix = f"{output}/{validator}-{verifier}"
    suffix = f"results.SV-COMP{year}_{subcategory}.xml.bz2{processed_fixed}"
    path = f"{prefix}.????-??-??_??-??-??.{suffix}"
    files = glob.glob(path)
    if not files:
        return None
    # extract datetime out of filename and sort by ascending by time
    files.sort(
        key=lambda name: datetime.strptime(
            str(name).replace(f"{prefix}.", "").replace(f".{suffix}", ""),
            "%Y-%m-%d_%H-%M-%S",
        )
    )
    assert len(files) > 0
    # take latest file
    latest_file = files[-1]
    return latest_file
