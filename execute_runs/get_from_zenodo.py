#!/usr/bin/env python3

import sys
import argparse
import httpx
import json
import hashlib
import logging


class ZenodoError(Exception):
    def __init__(self, message: str) -> None:
        super().__init__(message)


def get_json_from_zenodo(client: httpx.Client, doi: str):
    assert doi.startswith("10.5281/zenodo.")
    record_id = doi.replace("10.5281/zenodo.", "")
    url = f"https://zenodo.org/api/records/{record_id}"
    response = client.get(url)
    if response.status_code != 200:
        logging.error(f"Download failed with status: {response.status_code} ({url}).")
        logging.error(
            f"Please make sure to enter a DOI pointing to a version and redirecting to the latest version."
        )
        exit(1)
    return json.loads(response.content)


def parser():
    p = argparse.ArgumentParser(
        description="Return download URL for a given Zenodo DOI"
    )
    p.add_argument(
        "DOI",
        nargs=1,
        type=str,
        default="",
        help="DOI to get a download URL for",
    )
    p.add_argument(
        "--print-zenodo-json",
        dest="print_zenodo_json",
        action="store_true",
        default=False,
        help="print the JSON export of the Zenodo record instead",
    )
    # add argument for download to a given filename
    p.add_argument(
        "--download",
        dest="download",
        type=str,
        help="download the file to a given filename",
    )
    p.add_argument(
        "--require-file-type",
        dest="requireFileType",
        default="application/zip",
        help="require that the DOI points to the given file type, use * for all file types",
    )
    p.add_argument(
        "--retries",
        dest="retries",
        default=3,
        type=int,
        help="number of retries for downloading the file",
    )

    return p


def get_download_url(client: httpx.Client, doi: str):
    content = get_json_from_zenodo(client, doi)
    try:
        if len(content["files"]) != 1:
            raise ZenodoError(
                f"Found {len(content['files'])} files in Zenodo record but only one is allowed."
            )
        logging.debug(f'Found file: {content["files"][0]["links"]["self"]}')
        checksum = get_checksum(content["files"][0])
        if checksum:
            return content["files"][0]["links"]["self"], checksum
        else:
            raise ZenodoError(f"Error in fetching for {doi}, no checksum found.")
    except KeyError as e:
        raise ZenodoError(f"Error in fetching for {doi}, key not found: {e}")


# Check meta data and return expected checksum for given JSON record
def get_checksum(file_info):
    if not file_info["checksum"].startswith("md5:"):
        raise AssertionError("Checksum is not calculated with md5.")
    if not file_info["key"].endswith(".zip"):
        raise AssertionError("File is not a ZIP file.")
    return file_info["checksum"][len("md5:") :]


def download_file(
    client: httpx.Client, url: str, filename: str, expected_checksum: str
):
    response = client.get(url)
    if response.status_code != 200:
        raise ZenodoError(f"Bad status code: {response.status_code} ({url})")
    # calculate checksum of downloaded file
    checksum = hashlib.md5(response.content).hexdigest()
    logging.debug(
        f"Comparing received checksum {checksum} with expected checksum {expected_checksum}"
    )
    if checksum != expected_checksum:
        raise ZenodoError(
            f"Checksum of downloaded file does not match checksum in metadata."
        )
    with open(filename, "wb") as f:
        f.write(response.content)


def retry_download(
    client: httpx.Client, doi: str, fileType: str, filename: str, retries: int
):
    if filename:
        unique_file, checksum = get_download_url(client, doi)
        for i in range(retries):
            try:
                download_file(client, unique_file, filename, checksum)
                break
            except ZenodoError as e:
                logging.warning(
                    f"Error ('{e}') while downloading file, retrying ({i+1}/{retries})"
                )
                continue
        else:
            raise ZenodoError(f"Error in downloading file, exceeded {retries} retries.")
        logging.info(f"Success: downloaded {filename}")
    else:
        content = get_json_from_zenodo(client, doi)
        logging.info(
            f"Success: The DOI points to a file of type {fileType}. However, no filename was specified, use --download <filename> to download the file."
        )
        logging.info(
            f'Download the file on your own: {content["files"][0]["links"]["self"]}'
        )
        print(content["files"][0]["links"]["self"])


def main(argv=None):
    logging.basicConfig(format="%(levelname)s:%(message)s", level=logging.DEBUG)

    if argv is None:
        argv = sys.argv[1:]
    args = parser().parse_args(argv)

    client = httpx.Client(http2=True)
    if args.print_zenodo_json:
        try:
            content = get_json_from_zenodo(client, args.DOI[0])
            logging.info(json.dumps(content, indent=4))
        except ZenodoError as e:
            logging.error(e)
    else:
        try:
            retry_download(
                args.DOI[0], args.requireFileType, args.download, args.retries
            )
        except KeyError as e:
            raise ZenodoError(f"Error in fetching for {args.DOI[0]}, key not found {e}")


if __name__ == "__main__":
    sys.exit(main())
