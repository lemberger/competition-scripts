#!/bin/bash

#source "$(dirname "$0")"/../configure.sh
SCRIPT_DIR="$(dirname "$0")/.."
export PYTHONPATH="$SCRIPT_DIR/../benchexec"

COMPETITIONNAME="$1"
COMPETITIONYEAR="$2"
COMPETITIONTRACK="$3"
if [[ -z "$COMPETITIONNAME" || -z "$COMPETITIONNAME" || -z "$COMPETITIONTRACK" ]]; then
  echo "Usage: $0 <competition name> <competition year> <competition track>" >&2
  echo "Example: $0 'SV-COMP' '2024' 'Verification'" >&2
  exit 1
fi

for TOOL_FILE in fm-tools/data/*.yml; do
  TOOL="$(basename "${TOOL_FILE%.yml}")"

  echo >&2
  TOOL="$(basename "${TOOL_FILE%.yml}")"

  VERSION=$(yq --raw-output ".competition_participations [] \
                  | select(.competition == \"$COMPETITIONNAME $COMPETITIONYEAR\" and .track == \"$COMPETITIONTRACK\") \
                  | .tool_version" "$TOOL_FILE" 2>/dev/null)
  if [ -z "$VERSION" ]; then
    # echo "There is no version for '$TOOL' participating in '$COMPETITIONNAME $COMPETITIONYEAR'." >&2
    continue
  fi

  DOI=$(yq --raw-output ".versions [] \
                  | select(.version == \"$VERSION\") | .doi" "$TOOL_FILE" | grep -v "^jq: error")
  if [[ "$DOI" == "null" || "$DOI" == "" ]]; then
    echo "There is no DOI for '$TOOL' participating in '$COMPETITIONNAME $COMPETITIONYEAR'." >&2
    continue
  fi

  echo "Downloading archive for '$TOOL' ..." >&2
  "$SCRIPT_DIR"/execute_runs/update-archives.py \
      --fm-root "$SCRIPT_DIR"/../fm-tools/ \
      --archives-root "$SCRIPT_DIR"/../archives/ \
      --competition "$COMPETITIONNAME $COMPETITIONYEAR" \
      --competition-track "$COMPETITIONTRACK" \
      "$TOOL"
  RESULT=$?
  if [[ "$RESULT" != 0 ]]; then
    echo "Archive download for '$TOOL' failed." >&2
    continue;
  fi

  echo "Checking archive for '$TOOL' for '$COMPETITIONNAME' and track '$COMPETITIONTRACK' ..." >&2
  "$SCRIPT_DIR"/test/check-archive.py \
      --archives-root "$SCRIPT_DIR"/../archives/ \
      --competition-track "$COMPETITIONTRACK" \
      "$COMPETITIONNAME" \
      "$TOOL"
  RESULT=$?
  if [[ "$RESULT" != 0 ]]; then
    echo "Archive check for '$TOOL' failed." >&2
    continue;
  fi

  echo "Checked and usable tool: '$TOOL'" >&2

  echo "$TOOL"
done
