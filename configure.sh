# Configuration of variables to initialize the competition environment

set -eao pipefail

SCRIPT_DIR=$(realpath "scripts");
export BENCHMARKSDIR="benchmark-defs";

YEAR=$(yq --raw-output '.year' ${BENCHMARKSDIR}/category-structure.yml)
COMPETITIONNAME=$(yq --raw-output '.competition' ${BENCHMARKSDIR}/category-structure.yml)
COMPETITION=${COMPETITIONNAME}${YEAR#??}  # use two last digits of year, only
TRACK="Verification"
PRODUCER="verify"
if [[ $COMPETITIONNAME == "Test-Comp" ]]; then
  TRACK="Test Generation"
  PRODUCER="test"
fi
TARGETSERVER=`echo ${COMPETITIONNAME} | tr A-Z a-z`
export FILE_STORE_URL_PREFIX="https://${TARGETSERVER}.sosy-lab.org/${YEAR}/results/"

export PATHPREFIX=$(realpath .)
TARGETDIR=${COMPETITIONNAME}
export RESULTSVERIFICATION="results-verified";
export RESULTSVALIDATION="results-validated";
export BINDIR="bin";
export PYTHONPATH="$PATHPREFIX/benchexec:$SCRIPT_DIR";
export BENCHEXEC_PATH="${PATHPREFIX}/benchexec";
BENCHMARKSCRIPT="$(dirname "$0")/../../benchexec/bin/benchexec"
BENCHEXECOPTIONS="--maxLogfileSize 2MB --read-only-dir / --read-only-dir $PATHPREFIX --overlay-dir ./ --hidden-dir /home/"
if [ -e /data ]; then
  BENCHEXECOPTIONS="$BENCHEXECOPTIONS --hidden-dir /data/"
fi
if [ -e /localhome ]; then
  BENCHEXECOPTIONS="$BENCHEXECOPTIONS --hidden-dir /localhome/"
fi

ADDRESS_BOOK=~/.competition-address-book.txt
USER_CONFIG=~/.competition-configure.sh
if [ -e "$USER_CONFIG" ]; then
  source "$USER_CONFIG"
fi
VERIFIERCLOUD_CONFIG=~/.competition-configure-verifiercloud.sh
if [ -e "$VERIFIERCLOUD_CONFIG" ]; then
  source "$VERIFIERCLOUD_CONFIG"
else
  OPTIONSVERIFY="-N $(($(nproc) / 8))"    # Number of parallel executing verification/test runs
  OPTIONSVALIDATE="-N $(($(nproc) / 8))"  # Number of parallel executing validation runs
  # Suggested config for a quick and rough local execution
  # LIMIT_CORES="--limitCores 2"            # Number of cores for verification/test runs
  # LIMIT_VAL_CORES="--limitCores 2"        # Number of corse for validation runs
  # OPTIONSVERIFY="-N $(($(nproc) / 2))"    # Number of parallel executing verification/test runs
  # OPTIONSVALIDATE="-N $(($(nproc) / 2))"  # Number of parallel executing validation runs
fi

ACTIONS=${ACTIONS:-"PRODUCE_RESULTS VALIDATE_RESULTS PREPARE_RESULTS"}

export HASHES_BASENAME="fileHashes.json";
export HASHDIR_BASENAME="fileByHash";

export PROPERTIES=$(yq -r '.properties []' benchmark-defs/category-structure.yml)
#VALIDATORLIST=$(yq -r '.validators []' benchmark-defs/category-structure.yml);
VALIDATORLIST=$(for i in $("$SCRIPT_DIR"/execute_runs/list-tools.sh "$COMPETITIONNAME" "$YEAR" "Validation of Correctness Witnesses"); do echo "$i""-validate-correctness-witnesses"; done)
VALIDATORLIST="$VALIDATORLIST $(for i in $("$SCRIPT_DIR"/execute_runs/list-tools.sh "$COMPETITIONNAME" "$YEAR" "Validation of Violation Witnesses"); do echo "$i""-validate-violation-witnesses"; done)"
VALIDATORLIST="$VALIDATORLIST $(for i in $("$SCRIPT_DIR"/execute_runs/list-tools.sh "$COMPETITIONNAME" "$YEAR" "Validation of Test Suites"); do echo "$i""-validate-test-suites"; done)"
#VALIDATORLIST="witnesslint-validate-violation-witnesses";
#ACTIONS="PRODUCE_RESULTS VALIDATE_RESULTS"

RESULTSLEVEL="Final";
if [[ -n "$LIMIT_CORES" || -n "$LIMIT_MEMORY" || -n "$LIMIT_TIME" ]]; then
  LIMITSTEXT="\nLimits: The current pre-run results are limited with: $LIMIT_TIME $LIMIT_CORES $LIMIT_MEMORY.\n"
  RESULTSLEVEL="Pre-run";
fi

if [[ "${COMPETITIONNAME}" == "SV-COMP" ]]; then
  VALIDATIONKIND="witnesses";

  WITNESSTARGET="witness.graphml";
  WITNESSGLOBSUFFIX=".graphml";

elif [[ "${COMPETITIONNAME}" == "Test-Comp" ]]; then
  VALIDATIONKIND="test-suites";

  WITNESSTARGET="test-suite.zip";
  WITNESSGLOBSUFFIX=".zip";

  TESTCOMPOPTION="--zipResultFiles";

  TARGETDIR=`echo ${COMPETITIONNAME} | tr A-Z a-z`

else
  echo "Unhandled competition $COMPETITIONNAME" ; false
fi



